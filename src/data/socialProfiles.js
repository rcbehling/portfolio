import emailIcon from '../assets/email_icon.png';
import githubIcon from '../assets/github_icon.png';
import linkedinIcon from '../assets/linkedin_icon.png';
import twitterIcon from '../assets/twitter_icon.png';
 
const SOCIAL_PROFILES = [
    {
        id: 1,
        link: 'mailto:rogerio.behling@linx.com.br',
        image: emailIcon
    },
    {
        id: 2,
        link: 'https://gitlab.com/rcbehling/',
        image: githubIcon
    },
    {
        id: 3,
        link: 'http://www.linkedin.com/',
        image: linkedinIcon
    },
    {
        id: 4,
        link: 'http://twitter.com/',
        image: twitterIcon
    }
];
 
export default SOCIAL_PROFILES;

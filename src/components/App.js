import React , {Component} from 'react';
import Projects from './Projects';
import SocialProfiles from './SocialProfiles';
import profile from '../assets/profile.png';
import Title from './Title';

class App extends Component {
	state = { counter: 0 , displayBio: false};

	toggleDisplayBio = () => {
		this.setState({ displayBio: !this.state.displayBio });
	}

	addCounter = () => {
		this.setState({ counter: this.state.counter + 1 });
	}

	render() {
		return (
			<div>
				<img src={profile} alt='Profile Image' className='profile' />
				<h1>Meu portfolio!</h1>
				{
					this.state.displayBio ? (
						<div>
							<p>Olá, meu nome é Rogério Behling!</p>
							<p>Eu moro em Blumenau, sou analista de sistemas da Linx S.A.</p>
							<Title />
							<p>Este é meu primeiro app de exemplo para o treinamento React.</p>
							<button onClick={ this.toggleDisplayBio }>Ver Menos</button>
						</div>
					) : (
						<div>
							<button onClick={ this.toggleDisplayBio }>Ver Mais</button>
						</div>
					)
				}
				{/* <p>{ this.state.counter }</p> */}
				{/* <button onClick={ this.addCounter }>Add Counter</button> */}
				<hr />
				<Projects />
				<SocialProfiles />
			</div>
		)
	}
}

export default App;

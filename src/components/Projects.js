import React , {Component} from 'react';
import PROJECTS from '../data/projects';

class Project extends Component {
	render() {
		const { title, image, description, link } = this.props.project;
		return (
			<span style={{ display: 'inline-block' }}>
				<div>
					<h3>{title}</h3>
					<img src={image} style={{ width: 200, height: 200, margin: 10}} />
					<p>{description}</p>
					<a href={link}>{link}</a>
				</div>
			</span>
		)
	}
}

class Projects extends Component {
	render() {
		return (
			<div>
				<h1>Meus Projetos</h1>
				<div>
					{
						PROJECTS.map(PROJECT => {
							return <Project key={PROJECT.id} project={PROJECT}/>
						})
					}
				</div>
			</div>
		)
	}
}

export default Projects;
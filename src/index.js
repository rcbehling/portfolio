import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './index.css';


ReactDOM.render(
  <App />,
  document.getElementById('root')
);

class Animal {
  constructor(nome) {
    this.nome = nome;
  }

  falar(){
    console.log('Roar');
  }
}

class Humano extends Animal {
  constructor(nome, idade, cabelo) {
    super(nome);
    this.idade = idade;
    this.cabelo = cabelo;
  }

  falar(){
    console.log('Oi');
  }

  apresentar(){
    console.log('Meu nome é', this.nome, 'tenho', this.idade, 'anos e tenho cabelo', this.cabelo, '.');
  }
}

const a = new Animal('Leão');
console.log(a)
console.log(a.falar())

const h = new Humano('Rogério', '44', 'preto');
console.log(h);
console.log(h.falar());
console.log(h.apresentar());
